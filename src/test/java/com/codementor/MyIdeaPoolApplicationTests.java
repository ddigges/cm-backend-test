package com.codementor;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MyIdeaPoolApplication.class)
public class MyIdeaPoolApplicationTests {

    @Test
    public void contextLoads() {
    }
}
