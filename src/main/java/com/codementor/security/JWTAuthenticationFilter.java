package com.codementor.security;

import com.codementor.datatransferobject.LoginDTO;
import com.codementor.datatransferobject.TokenDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * Authenticates request during the login flow.
 */
public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private AuthenticationManager authenticationManager;

    private ObjectMapper objectMapper;

    private JwtTokenFactory jwtTokenFactory;


    public JWTAuthenticationFilter(AuthenticationManager authenticationManager, ObjectMapper mapper, JwtTokenFactory jwtTokenFactory) {
        setAuthenticationManager(authenticationManager);
        this.authenticationManager = authenticationManager;
        this.objectMapper = mapper;
        this.jwtTokenFactory = jwtTokenFactory;
    }


    @Override
    public Authentication attemptAuthentication(
        HttpServletRequest req,
        HttpServletResponse res) throws AuthenticationException {
        try {
            LoginDTO loginDTO = new ObjectMapper()
                .readValue(req.getInputStream(), LoginDTO.class);

            return authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                    loginDTO.getEmail(),
                    loginDTO.getPassword(),
                    new ArrayList<>())
            );
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    protected void successfulAuthentication(
        HttpServletRequest req,
        HttpServletResponse res,
        FilterChain chain,
        Authentication auth) throws IOException, ServletException {

        String accessToken = jwtTokenFactory.createJwtToken(((User) auth.getPrincipal()).getUsername());
        String refreshToken = jwtTokenFactory.createRefreshToken(((User) auth.getPrincipal()).getUsername());

        TokenDTO tokenDTO = new TokenDTO();
        tokenDTO.setJwt(accessToken);
        tokenDTO.setRefresh_token(refreshToken);

        res.setStatus(HttpStatus.OK.value());
        res.setContentType(MediaType.APPLICATION_JSON_VALUE);
        objectMapper.writeValue(res.getWriter(), tokenDTO);
    }
}