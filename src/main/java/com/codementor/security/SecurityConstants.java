package com.codementor.security;

public class SecurityConstants {
    public static final String HEADER_STRING = "X-Access-Token";
    public static final String SIGN_UP_URL = "/users";
    public static final String LOGIN_URL = "/access-tokens";
    public static final String TOKENS_URL = "/access-tokens/**";
}
