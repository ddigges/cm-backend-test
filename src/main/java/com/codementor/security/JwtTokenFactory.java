package com.codementor.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Generates access tokens and refresh tokens.
 */
@Component
public class JwtTokenFactory {
    private final JwtSettings settings;


    @Autowired
    public JwtTokenFactory(JwtSettings settings) {
        this.settings = settings;
    }


    /**
     * Creates an access token.
     *
     * @param userName
     * @return
     */
    public String createJwtToken(String userName) {
        LocalDateTime currentTime = LocalDateTime.now();

        String token = Jwts.builder()
            .setIssuer(settings.getTokenIssuer())
            .setSubject(userName)
            .setExpiration(Date.from(currentTime
                .plusMinutes(settings.getTokenExpirationTime())
                .atZone(ZoneId.systemDefault()).toInstant()))
            .signWith(SignatureAlgorithm.HS512, settings.getTokenSigningKey())
            .compact();
        return token;
    }


    /**
     * Creates a refresh token.
     *
     * @param userName
     * @return
     */
    public String createRefreshToken(String userName) {
        LocalDateTime currentTime = LocalDateTime.now();

        String token = Jwts.builder()
            .setIssuer(settings.getTokenIssuer())
            .setSubject(userName)
            .setExpiration(Date.from(currentTime
                .plusMinutes(settings.getRefreshTokenExpTime())
                .atZone(ZoneId.systemDefault()).toInstant()))
            .signWith(SignatureAlgorithm.HS512, settings.getTokenSigningKey())
            .compact();

        return token;
    }
}
