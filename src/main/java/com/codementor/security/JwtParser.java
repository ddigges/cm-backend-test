package com.codementor.security;

import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Responsible for parsing the subject from the token.
 */
@Component
public class JwtParser {
    private JwtSettings jwtSettings;


    @Autowired
    public JwtParser(JwtSettings jwtSettings) {
        this.jwtSettings = jwtSettings;
    }


    /**
     * Parses the subject from the JWT.
     * @param token
     * @return
     */
    public String parseRefreshToken(String token) {
        if (token != null) {
            String user = Jwts.parser()
                .setSigningKey(jwtSettings.getTokenSigningKey())
                .parseClaimsJws(token)
                .getBody()
                .getSubject();

            return user;
        }
        return null;
    }
}
