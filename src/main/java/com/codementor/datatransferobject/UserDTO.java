package com.codementor.datatransferobject;

import com.codementor.validation.ValidPassword;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;

/**
 * User resource.
 */
public class UserDTO {
    @Email
    private String email;

    @NotNull
    @Size(min = 4)
    private String name;

    @ValidPassword
    private String password;

    @Null
    private String avatar_url;


    public String getEmail() {
        return email;
    }


    public void setEmail(String email) {
        this.email = email;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    @JsonIgnore
    public String getPassword() {
        return password;
    }


    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }


    public String getAvatar_url() {
        return avatar_url;
    }


    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }
}
