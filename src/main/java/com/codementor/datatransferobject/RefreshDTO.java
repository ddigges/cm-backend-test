package com.codementor.datatransferobject;

import javax.validation.constraints.NotNull;

/**
 * Refresh token requests from clients
 */
public class RefreshDTO {
    @NotNull
    private String refresh_token;


    public String getRefresh_token() {
        return refresh_token;
    }


    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

}
