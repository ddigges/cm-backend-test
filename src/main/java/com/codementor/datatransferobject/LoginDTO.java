package com.codementor.datatransferobject;

import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Email;

/**
 * Login request from clients.
 */
public class LoginDTO {
    @Email
    private String email;

    @NotNull
    private String password;


    public String getEmail() {
        return email;
    }


    public void setEmail(String email) {
        this.email = email;
    }


    public String getPassword() {
        return password;
    }


    public void setPassword(String password) {
        this.password = password;
    }
}
