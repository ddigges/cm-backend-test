package com.codementor.datatransferobject;

/**
 * Refresh token response.
 */
public class RefreshResponseDTO {
    private String jwt;


    public String getJwt() {
        return jwt;
    }


    public void setJwt(String jwt) {
        this.jwt = jwt;
    }
}
