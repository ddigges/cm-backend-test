package com.codementor.datatransferobject;

import com.fasterxml.jackson.annotation.JsonInclude;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Size;

/**
 * Idea representation communicated to clients.
 *
 * @author ddigges
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class IdeaDTO {
    @Null
    private String id;

    @NotNull
    @Size(min = 1)
    private String content;

    @NotNull
    @Min(1)
    @Max(10)
    private Integer impact;

    @NotNull
    @Min(1)
    @Max(10)
    private Integer ease;

    @NotNull
    @Min(1)
    @Max(10)
    private Integer confidence;

    @Null
    private Double average_score;

    private Long created_at;


    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }


    public String getContent() {
        return content;
    }


    public void setContent(String content) {
        this.content = content;
    }


    public Integer getImpact() {
        return impact;
    }


    public void setImpact(Integer impact) {
        this.impact = impact;
    }


    public Integer getEase() {
        return ease;
    }


    public void setEase(Integer ease) {
        this.ease = ease;
    }


    public Integer getConfidence() {
        return confidence;
    }


    public void setConfidence(Integer confidence) {
        this.confidence = confidence;
    }


    public Double getAverage_score() {
        return average_score;
    }


    public void setAverage_score(Double average_score) {
        this.average_score = average_score;
    }


    public Long getCreated_at() {
        return created_at;
    }


    public void setCreated_at(Long created_at) {
        this.created_at = created_at;
    }


}
