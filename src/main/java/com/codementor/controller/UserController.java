package com.codementor.controller;

import com.codementor.datatransferobject.TokenDTO;
import com.codementor.datatransferobject.UserDTO;
import com.codementor.domainobject.UserDO;
import com.codementor.exception.ConstraintsViolationException;
import com.codementor.exception.EntityNotFoundException;
import com.codementor.mapper.UserMapper;
import com.codementor.security.JwtTokenFactory;
import com.codementor.service.user.UserService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * All operations with a user will be routed by this controller.
 * <p/>
 *
 * @author ddigges
 */
@RestController
@RequestMapping
public class UserController {
    private final UserService userService;
    private final UserMapper userMapper;
    private final JwtTokenFactory jwtTokenFactory;


    @Autowired
    public UserController(final UserService userService, final UserMapper userMapper, final JwtTokenFactory jwtTokenFactory) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.jwtTokenFactory = jwtTokenFactory;
    }


    @PostMapping("/users")
    public TokenDTO createUser(@Valid @RequestBody UserDTO userDTO) throws ConstraintsViolationException {
        UserDO userDO = userMapper.dtoTodo(userDTO);
        userMapper.doToDto(userService.createUser(userDO));

        String accessToken = jwtTokenFactory.createJwtToken(userDO.getEmail());
        String refreshToken = jwtTokenFactory.createRefreshToken(userDO.getEmail());

        TokenDTO tokenDTO = new TokenDTO();
        tokenDTO.setJwt(accessToken);
        tokenDTO.setRefresh_token(refreshToken);
        return tokenDTO;
    }


    @GetMapping("/me")
    public UserDTO getCurrentUser() throws EntityNotFoundException {
        String emailId = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userMapper.doToDto(userService.findByEmail(emailId));
    }
}
