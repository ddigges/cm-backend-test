package com.codementor.controller;

import com.codementor.datatransferobject.IdeaDTO;
import com.codementor.domainobject.IdeaDO;
import com.codementor.exception.ConstraintsViolationException;
import com.codementor.exception.EntityNotFoundException;
import com.codementor.exception.UnAuthorizedException;
import com.codementor.mapper.IdeaMapper;
import com.codementor.service.idea.IdeaService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * All operations with an idea will be routed by this controller.
 * <p/>
 *
 * @author ddigges
 */
@RestController
@RequestMapping("ideas")
public class IdeaController {

    private final IdeaService ideaService;

    private final IdeaMapper ideaMapper;


    @Autowired
    public IdeaController(IdeaService ideaService, IdeaMapper ideaMapper) {
        this.ideaService = ideaService;
        this.ideaMapper = ideaMapper;
    }


    @GetMapping
    public List<IdeaDTO> getIdeas(@RequestParam(defaultValue = "1", name = "page") int pageNumber, @RequestParam(defaultValue = "10") int pageSize) throws EntityNotFoundException {
        String userEmail = getLoggedInUser();
        return ideaMapper.doToDto(ideaService.getIdeas(pageNumber, pageSize, userEmail));
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public IdeaDTO createIdea(@Valid @RequestBody IdeaDTO ideaDTO) throws ConstraintsViolationException, EntityNotFoundException {
        String userEmail = getLoggedInUser();
        IdeaDO ideaDO = ideaMapper.dtoToDo(ideaDTO);
        return ideaMapper.doToDto(ideaService.createIdea(userEmail, ideaDO));
    }


    @PutMapping("/{ideaId}")
    public IdeaDTO updateIdea(
        @Valid @PathVariable long ideaId, @Valid @RequestBody IdeaDTO ideaDTO)
        throws ConstraintsViolationException, EntityNotFoundException, UnAuthorizedException {
        String userEmail = getLoggedInUser();
        IdeaDO ideaDO = ideaMapper.dtoToDo(ideaDTO);
        return ideaMapper.doToDto(ideaService.updateIdea(userEmail, ideaId, ideaDO));
    }


    @DeleteMapping("/{ideaId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteIdea(@Valid @PathVariable long ideaId) throws EntityNotFoundException, UnAuthorizedException {
        String userEmail = getLoggedInUser();
        ideaService.deleteIdea(userEmail, ideaId);
    }


    private String getLoggedInUser() {
        return (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
