package com.codementor.controller;

import com.codementor.datatransferobject.RefreshDTO;
import com.codementor.datatransferobject.RefreshResponseDTO;
import com.codementor.security.JwtParser;
import com.codementor.security.JwtTokenFactory;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * All operations with related to auth will be routed by this controller.
 * <p/>
 */
@RestController
@RequestMapping("access-tokens")
public class AuthController {

    private final JwtParser jwtParser;

    private final JwtTokenFactory jwtTokenFactory;


    @Autowired
    public AuthController(final JwtParser jwtParser, final JwtTokenFactory jwtTokenFactory) {
        this.jwtParser = jwtParser;
        this.jwtTokenFactory = jwtTokenFactory;
    }


    @PostMapping("/refresh")
    public RefreshResponseDTO refreshToken(@Valid @RequestBody RefreshDTO refreshDTO) {
        String refreshToken = refreshDTO.getRefresh_token();
        String user = jwtParser.parseRefreshToken(refreshToken);
        RefreshResponseDTO tokenDTO = new RefreshResponseDTO();
        if (user != null) {
            tokenDTO.setJwt(jwtTokenFactory.createJwtToken(user));
        }
        return tokenDTO;
    }
}
