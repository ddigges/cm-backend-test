package com.codementor.domainobject;

import java.time.ZonedDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table
public class IdeaDO {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private ZonedDateTime dateCreated = ZonedDateTime.now();

    @Column(nullable = false)
    private String content;

    @Column(nullable = false)
    @Max(10)
    @Min(1)
    private Integer impact;

    @Column(nullable = false)
    @Max(10)
    @Min(1)
    private Integer ease;

    @Column(nullable = false)
    @Max(10)
    @Min(1)
    private Integer confidence;

    @Column(nullable = false)
    @Max(10)
    @Min(1)
    private Double averageScore;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false)
    private UserDO user;


    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }


    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }


    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }


    public String getContent() {
        return content;
    }


    public void setContent(String content) {
        this.content = content;
    }


    public Integer getImpact() {
        return impact;
    }


    public void setImpact(Integer impact) {
        this.impact = impact;
    }


    public Integer getEase() {
        return ease;
    }


    public void setEase(Integer ease) {
        this.ease = ease;
    }


    public Integer getConfidence() {
        return confidence;
    }


    public void setConfidence(Integer confidence) {
        this.confidence = confidence;
    }


    public Double getAverageScore() {
        return averageScore;
    }


    public void setAverageScore(Double averageScore) {
        this.averageScore = averageScore;
    }


    public UserDO getUser() {
        return user;
    }


    public void setUser(UserDO user) {
        this.user = user;
    }
}
