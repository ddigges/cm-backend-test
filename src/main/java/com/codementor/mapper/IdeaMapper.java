package com.codementor.mapper;

import com.codementor.datatransferobject.IdeaDTO;
import com.codementor.domainobject.IdeaDO;
import java.util.List;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;

/**
 * Maps between {@link IdeaDTO} and {@link IdeaDO}
 * representations of an idea.
 */
@Mapper
public abstract class IdeaMapper {
    public abstract IdeaDO dtoToDo(IdeaDTO ideaDTO);

    @Mappings({
        @Mapping(source = "averageScore", target = "average_score")
    })
    public abstract IdeaDTO doToDto(IdeaDO ideaDO);


    @AfterMapping
    public void doToDto(IdeaDO source, @MappingTarget IdeaDTO target) {
        target.setCreated_at(source.getDateCreated().toEpochSecond());
        target.setId(source.getId().toString());
    }


    public abstract List<IdeaDTO> doToDto(List<IdeaDO> ideaDOList);

}
