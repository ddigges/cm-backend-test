package com.codementor.mapper;

import com.codementor.datatransferobject.UserDTO;
import com.codementor.domainobject.UserDO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * Maps between {@link UserDO} and {@link UserDTO}
 * representations of a user.
 */
@Mapper
public interface UserMapper {
    UserDO dtoTodo(UserDTO userDTO);

    @Mappings({
        @Mapping(source = "avatarUrl", target = "avatar_url")
    })
    UserDTO doToDto(UserDO userDO);
}
