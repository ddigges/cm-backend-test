package com.codementor.dataaccessobject;

import com.codementor.domainobject.IdeaDO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Database Access Object for idea table.
 * <p/>
 */
public interface IdeaRepository extends CrudRepository<IdeaDO, Long>, PagingAndSortingRepository<IdeaDO, Long> {
    Page<IdeaDO> findAllByUserEmail(String userEmail, Pageable pageable);
}
