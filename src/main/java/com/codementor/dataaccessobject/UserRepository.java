package com.codementor.dataaccessobject;

import com.codementor.domainobject.UserDO;
import org.springframework.data.repository.CrudRepository;

/**
 * Database Access Object for user table.
 * <p/>
 */
public interface UserRepository extends CrudRepository<UserDO, Long> {
    UserDO findByEmail(String email);

    boolean existsByEmail(String email);
}
