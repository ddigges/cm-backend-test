package com.codementor.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNAUTHORIZED, reason = "Not authorized to perform this action")
public class UnAuthorizedException extends Exception {
    private static final long serialVersionUID = -1387516993334229948L;

    public UnAuthorizedException(String message) {
        super(message);
    }
}
