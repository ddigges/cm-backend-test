package com.codementor.service.avatar;

public interface ImageService {
    String getImageUrl(String emailId);
}
