package com.codementor.service.avatar;

import java.text.MessageFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * This is the class that performs operations related to Gravatar images.
 */
@Component
public class DefaultImageService implements ImageService {
    private static final String GRAVATAR_IMAGE_URL = "https://www.gravatar.com/avatar/{0}";


    /**
     * Computes the gravatar URL from the given emailId.
     * @param emailId
     * @return
     */
    @Override
    public String getImageUrl(String emailId) {
        return MessageFormat.format(GRAVATAR_IMAGE_URL, buildEmailHash(emailId));
    }


    private String buildEmailHash(String emailId) {
        return MD5(emailId.trim().toLowerCase());
    }


    public String MD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        }
        catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }
}
