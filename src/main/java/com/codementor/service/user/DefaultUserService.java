package com.codementor.service.user;

import com.codementor.dataaccessobject.UserRepository;
import com.codementor.domainobject.UserDO;
import com.codementor.exception.ConstraintsViolationException;
import com.codementor.exception.EntityNotFoundException;
import com.codementor.service.avatar.ImageService;
import com.codementor.service.common.LookupUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * Service to encapsulate the link between DAO and controller and to have business logic for User
 * specific things.
 * <p/>
 */
@Service
public class DefaultUserService implements UserService {
    private static Logger LOG = LoggerFactory.getLogger(DefaultUserService.class);
    private final UserRepository userRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private ImageService imageService;


    public DefaultUserService(final UserRepository userRepository, final BCryptPasswordEncoder bCryptPasswordEncoder, final ImageService imageService) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.imageService = imageService;
    }


    /**
     * Creates a new user.
     * @param userDO: details of the user to be created.
     * @return
     * @throws ConstraintsViolationException
     */
    @Override
    public UserDO createUser(UserDO userDO) throws ConstraintsViolationException {
        if (userRepository.existsByEmail(userDO.getEmail())) {
            throw new ConstraintsViolationException("Email already in use.");
        }

        userDO.setPassword(bCryptPasswordEncoder.encode(userDO.getPassword()));
        userDO.setAvatarUrl(imageService.getImageUrl(userDO.getEmail()));
        try {
            userDO = userRepository.save(userDO);
        }
        catch (DataIntegrityViolationException e) {
            LOG.warn("Some constraints are thrown due to user creation", e);
            throw new ConstraintsViolationException(e.getMessage());
        }
        return userDO;
    }


    /**
     * Looks up a user by id.
     * @param userId: id of the user to be looked up.
     * @return
     * @throws EntityNotFoundException: if no user with the given id exists.
     */
    @Override
    public UserDO getUser(Long userId) throws EntityNotFoundException {
        return LookupUtil.findUserChecked(userRepository, userId);
    }


    /**
     * Looks up a user by email.
     * @param emailId
     * @return
     * @throws EntityNotFoundException: if no user with the given email id exists.
     */
    @Override
    public UserDO findByEmail(String emailId) throws EntityNotFoundException {
        return LookupUtil.findUserCheckedByEmail(userRepository, emailId);
    }
}
