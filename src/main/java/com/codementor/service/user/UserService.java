package com.codementor.service.user;

import com.codementor.domainobject.UserDO;
import com.codementor.exception.ConstraintsViolationException;
import com.codementor.exception.EntityNotFoundException;

public interface UserService {
    UserDO createUser(UserDO userDO) throws ConstraintsViolationException;

    UserDO getUser(Long userId) throws EntityNotFoundException;

    UserDO findByEmail(String emailId) throws EntityNotFoundException;
}
