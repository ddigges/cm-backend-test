package com.codementor.service.user;

import com.codementor.dataaccessobject.UserRepository;
import com.codementor.domainobject.UserDO;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import static java.util.Collections.emptyList;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserRepository applicationUserRepository;


    public UserDetailsServiceImpl(final UserRepository applicationUserRepository) {
        this.applicationUserRepository = applicationUserRepository;
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDO userDO = applicationUserRepository.findByEmail(username);
        if (userDO == null) {
            throw new UsernameNotFoundException(username);
        }
        return new User(userDO.getEmail(), userDO.getPassword(), emptyList());
    }
}