package com.codementor.service.common;

import com.codementor.dataaccessobject.IdeaRepository;
import com.codementor.dataaccessobject.UserRepository;
import com.codementor.domainobject.IdeaDO;
import com.codementor.domainobject.UserDO;
import com.codementor.exception.EntityNotFoundException;

/**
 * Common utility for looking up entities.
 */
public class LookupUtil {
    /**
     * Looks up user with the given id.
     *
     * @param userId: user id to be looked up.
     * @return
     * @throws EntityNotFoundException if user with the given id is not found.
     */
    public static UserDO findUserChecked(UserRepository userRepository, Long userId) throws EntityNotFoundException {
        UserDO userDO = userRepository.findOne(userId);

        if (userDO == null) {
            throw new EntityNotFoundException("User with the given id was not found.");
        }
        return userDO;
    }


    /**
     * Looks up user with the given id.
     *
     * @param email: emailId to look up
     * @return
     * @throws EntityNotFoundException if user with the given id is not found.
     */
    public static UserDO findUserCheckedByEmail(UserRepository userRepository, String email) throws EntityNotFoundException {
        UserDO userDO = userRepository.findByEmail(email);

        if (userDO == null) {
            throw new EntityNotFoundException("User with the given id was not found.");
        }
        return userDO;
    }


    /**
     * Looks up idea with the given id.
     *
     * @param ideaId: idea id to be looked up
     * @return
     * @throws EntityNotFoundException if idea with the given id is not found.
     */
    public static IdeaDO findIdeaChecked(IdeaRepository ideaRepository, Long ideaId) throws EntityNotFoundException {
        IdeaDO ideaDO = ideaRepository.findOne(ideaId);

        if (ideaDO == null) {
            throw new EntityNotFoundException("Idea with the given id was not found.");
        }
        return ideaDO;
    }
}
