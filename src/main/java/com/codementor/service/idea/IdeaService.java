package com.codementor.service.idea;

import com.codementor.domainobject.IdeaDO;
import com.codementor.exception.ConstraintsViolationException;
import com.codementor.exception.EntityNotFoundException;
import com.codementor.exception.UnAuthorizedException;
import java.util.List;
import org.hibernate.exception.ConstraintViolationException;

public interface IdeaService {
    List<IdeaDO> getIdeas(int pageNumber, int pageSize, String userEmail) throws ConstraintViolationException;

    IdeaDO createIdea(String emailId, IdeaDO ideaDO) throws ConstraintsViolationException, EntityNotFoundException;

    IdeaDO updateIdea(String userEmail, Long id, IdeaDO ideaDO) throws EntityNotFoundException, ConstraintsViolationException, UnAuthorizedException;

    void deleteIdea(String userEmail, Long id) throws EntityNotFoundException, UnAuthorizedException;
}
