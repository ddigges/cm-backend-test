package com.codementor.service.idea;

import com.codementor.dataaccessobject.IdeaRepository;
import com.codementor.dataaccessobject.UserRepository;
import com.codementor.domainobject.IdeaDO;
import com.codementor.domainobject.IdeaDO_;
import com.codementor.domainobject.UserDO;
import com.codementor.exception.ConstraintsViolationException;
import com.codementor.exception.EntityNotFoundException;
import com.codementor.exception.UnAuthorizedException;
import com.codementor.service.common.LookupUtil;
import java.util.List;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 * Service to encapsulate the link between DAO and controller and to have business logic for Idea
 * specific things.
 * <p/>
 */
@Service
public class DefaultIdeaService implements IdeaService {
    private static Logger LOG = LoggerFactory.getLogger(DefaultIdeaService.class);

    private IdeaRepository ideaRepository;

    private UserRepository userRepository;


    public DefaultIdeaService(IdeaRepository ideaRepository, UserRepository userRepository) {
        this.ideaRepository = ideaRepository;
        this.userRepository = userRepository;
    }

    /**
     * Returns a paginated list of ideas.
     *
     * @param pageNumber
     * @param pageSize
     * @return
     * @throws ConstraintViolationException
     */
    @Override
    public List<IdeaDO> getIdeas(int pageNumber, int pageSize, String userEmail) throws ConstraintViolationException {
        PageRequest pageRequest = new PageRequest(pageNumber - 1, pageSize, Sort.Direction.DESC, IdeaDO_.averageScore.getName());
        return ideaRepository.findAllByUserEmail(userEmail, pageRequest).getContent();
    }


    /**
     * Creates an idea for a given user.
     *
     * @param emailId
     * @param ideaDO
     * @return
     * @throws ConstraintViolationException some constraints are violated on the idea fields.
     * @throws EntityNotFoundException      if a user with the given ID is not found.
     */
    @Override
    public IdeaDO createIdea(String emailId, IdeaDO ideaDO) throws ConstraintsViolationException, EntityNotFoundException {
        UserDO userDO = LookupUtil.findUserCheckedByEmail(userRepository, emailId);
        ideaDO.setUser(userDO);
        ideaDO.setAverageScore(computeAverage(ideaDO));
        try {
            ideaDO = ideaRepository.save(ideaDO);
        }
        catch (DataIntegrityViolationException exception) {
            LOG.warn("Some constraints are thrown due to idea creation", exception);
            throw new ConstraintsViolationException(exception.getMessage());
        }
        return ideaDO;
    }


    /**
     * Updates the idea with given id with the new details passed in `ideaDO`.
     *
     * @param id
     * @param ideaDO
     * @return
     * @throws EntityNotFoundException:       if an idea with the given ID is not found.
     * @throws ConstraintsViolationException: some constraints are violated on the idea fields.
     */
    @Override
    public IdeaDO updateIdea(String userEmail, Long id, IdeaDO ideaDO) throws EntityNotFoundException, ConstraintsViolationException, UnAuthorizedException {
        IdeaDO existingIdea = LookupUtil.findIdeaChecked(ideaRepository, id);

        authorize(userEmail, existingIdea);

        existingIdea.setContent(ideaDO.getContent());
        existingIdea.setConfidence(ideaDO.getConfidence());
        existingIdea.setImpact(ideaDO.getImpact());
        existingIdea.setEase(ideaDO.getEase());
        existingIdea.setAverageScore(computeAverage(ideaDO));
        try {
            ideaDO = ideaRepository.save(existingIdea);
        }
        catch (DataIntegrityViolationException exception) {
            LOG.warn("Some constraints are thrown due to idea updation", exception);
            throw new ConstraintsViolationException(exception.getMessage());
        }
        return ideaDO;
    }


    /**
     * Deletes the idea with the given id.
     *
     * @param id
     * @throws EntityNotFoundException: if an idea with the given ID is not found.
     */
    @Override
    public void deleteIdea(String userEmail, Long id) throws EntityNotFoundException, UnAuthorizedException {
        IdeaDO ideaDO = LookupUtil.findIdeaChecked(ideaRepository, id);

        authorize(userEmail, ideaDO);

        ideaRepository.delete(ideaDO);
    }


    /**
     * Check if the given user is authorized to perform the action.
     *
     * @param userEmail
     * @param ideaDO
     * @throws UnAuthorizedException
     */
    private void authorize(String userEmail, IdeaDO ideaDO) throws UnAuthorizedException {
        if (ideaDO.getUser() == null || ideaDO.getUser().getEmail() == null || !ideaDO.getUser().getEmail().equals(userEmail)) {
            throw new UnAuthorizedException("Unauthorized");
        }
    }


    /**
     * Returns the average of the impactScore, easeScore, and confidenceScore.
     *
     * @param ideaDO
     * @return
     */
    private Double computeAverage(IdeaDO ideaDO) {
        return roundOff((ideaDO.getImpact() + ideaDO.getConfidence() + ideaDO.getEase()) / 3.0);
    }


    /**
     * round off to two decimal places
     *
     * @param value
     * @return
     */
    private Double roundOff(Double value) {
        return Math.round(value * 100) / 100D;
    }

}
